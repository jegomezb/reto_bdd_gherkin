# language: es

Característica: Solicitud de Crédito de Libre Inversión
  Como usuario de banco
  necesito solicitar un crédito de libre inversión
  para poder remodelar mi casa

  Escenario: Sección de solicitud del Crédito de Libre Inversión
        Dado que el usuario ingresó a su cuenta en la sucursal virtual
      Cuando el usuario selecciona la opción de solictar crédito de libre inversión
    Entonces se mostrará la sección para seleccionar el tipo de crédito que desea solicitar

  Escenario: Solicitud del Crédito de Libre Inversión exitoso
        Dado que el usuario seleccionó un tipo de crédito de libre inversión
      Cuando el usuario ingresa la información obligatoria requerida
    Entonces se mostrará un mensaje que el dinero será depositado en su cuenta en un tiempo determinado

  Escenario: Solicitud del Crédito de Libre Inversión fallido
        Dado que el usuario seleccionó un tipo de crédito de libre inversión
      Cuando el usuario ingresa la información obligatoria requerida
    Entonces se mostrará un mensaje que indica que no es posible realizar el préstamos por ciertos motivos