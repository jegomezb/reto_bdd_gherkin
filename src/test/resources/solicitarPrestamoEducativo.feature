# language: es

Característica: Solicitud de crédito estudiantil
  como usuario de banco
  necesito realizar un crédito estudiantil
  para continuar con mi formación académica

  Escenario: Verificar la opción de crédito estudiantil
    Dado que un usuario inicia sesión en la sucursal virtual
    Cuando el usuario selecciona la opción de crédito estudiantil
    Entonces visulizará la información correspondiente

  Escenario: Solicitud del crédito estudiantil
    Dado que un usuario solicitó un crédito estudiantil
    Cuando el usuario completa la información requerida y confirma
    Entonces se mostrará un mensaje indicando que su solicitud fue recibida