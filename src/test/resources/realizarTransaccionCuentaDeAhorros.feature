# language: es

Característica: Transacción en cuenta de ahorros
  Como usuario de banco
  necesito realizar una transacción en mi cuenta de ahorros
  para administrar mi dinero

  Escenario: Visualizar el saldo disponible
    Dado que el usuario ingresó a su cuenta en la sucursal virtual
    Cuando el usuario selecciona la opción de saldo disponible
    Entonces se mostrará el saldo disponible en la cuenta de ahorros

  Escenario: Transferir dinero desde una cuenta de ahorros con saldo suficiente
    Dado que el usuario seleccionó su cuenta de ahorros
    Y tiene saldo suficiente en su cuenta de ahorros
    Cuando el usuario selecciona que desea realizar una transferencia
    E ingresa la cantidad a transferir
    E ingresa la cuenta bancaría de destino
    Entonces se monstrará un mensaje de transacción exitosa
    Y se restará la cantidad a transferir del saldo disponible

  Escenario: Transferir dinero desde una cuenta de ahorros sin saldo suficiente
    Dado que el usuario seleccionó su cuenta de ahorros
    Y no tiene saldo suficiente en su cuenta de ahorros
    Cuando el usuario selecciona que desea realizar una transferencia
    E ingresa la cantidad a transferir
    E ingresa la cuenta bancaría de destino
    Entonces se mostrará un mensaje que indica que no es posible realizar la transacción por saldo insuficiente