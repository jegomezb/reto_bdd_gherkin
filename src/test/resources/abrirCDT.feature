# language: es

Característica: Creación de cuenta CDT
  Como usuario de banco
  necesito crear una cuenta CDT
  para poder incrementar mis ahorros en un plazo determinado

  Escenario: Sección de creación de cuenta CDT
        Dado que el usuario ingresó a su cuenta en la sucursal virtual
      Cuando el usuario selecciona la opción de crear CDT
    Entonces se mostrará la sección para que seleccione el tipo de CDT que desea abrir

  Escenario: Creación de cuenta CDT
        Dado que el usuario seleccionó un tipo de CDT
      Cuando ingrese la información obligtoria solicitada
    Entonces se mostrará un mensaje de confirmación de apertura del CDT