# language: es

Característica: Realizar pagos con cuenta corriente
  como usuario de banco
  necesito realizar el pago de servicios con mi cuenta corriente
  para estar al día en mis pagos

  Escenario: Verificar el saldo disponible
    Dado que un usuario ingresó en la aplicación bancaria
    Cuando el usuario selecciona la cuenta asociada
    Entonces se mostrará el saldo disponible en la cuenta corriente

  Escenario: Realizar pagos de servicios
    Dado que un usuario seleccionó su cuenta corriente
    Y visualiza que tiene saldo disponible en su cuenta corriente
    Cuando el usuario selecciona la opción de pagar un servicio
    Y confirma realizar el pago del servicio
    Entonces se mostrará un mensaje indicando que se realizo el pago del servicio exitosamente

  Escenario: Realizar pagos de servicios con saldo insuficiente
    Dado que un usuario seleccionó su cuenta corriente
    Y no cuenta con saldo disponible en su cuenta corriente
    Cuando el usuario selecciona la opcion de pagar un servicio
    Y confirma realizar el pago del servicio
    Entonces se mostrará un mensaje indicando que no cuenta con saldo suficiente para realizar el pago

